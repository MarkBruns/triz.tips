---
title: Fitness suggestor
subtitle: Move reminder, TABATA suggestor, WOD suggestor
comments: false
---

Like time mgmt app, except exercise NEEDS to happen ... and you have to drive the app, because you also drive the excuses ... the KEY there is overcoming your own excuse factory [which has benefits for everything].  

The reason that exercise matters is certainly not JUST the physical fitness aspects.  People miss the point, get this wrong -- it's the discipline-building parts of getting after your weakest area of fitness throughout the day.

DISCIPLINE = FREEDOM.