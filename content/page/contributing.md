---
title: Contributing 
subtitle: Join the asynch.WORK community
comments: false
---

THIS OPEN SOURCE PROJECT IS SO STEALTH THAT ... even our [contributing.md](https://gitlab.com/MarkBruns/gygbe/-/blob/master/CONTRIBUTING.md) page is not even really a plagiarized copy of a decent [*contributing.md*](https://mozillascience.github.io/working-open-workshop/contributing/), ie we haven't yet even decided WHO to steal from -- we have decided that we will shamelessly steal.

Until there's more, feel free to visit or clone the [GYGbe [blog/cms] repository](https://gitlab.com/MarkBruns/gygbe))

# PHILOSOPHY applied to the topic of CONTRIBUTING.

1) We can iterate faster when we innovate together, although the goal is not necessarily speed ... the goal is KNOWLEDGE ... when capable people do things that they are passionate about together there are bound to be arguements, ie very different points of view going back and forth, or arguing, is the basis of what makes LOVE fun.

2) The point or arguing is not the sport of arguing or being confrontational. This project is not some sort of *arguer hook-up method.*  The point of arguing is that knowledge is ALWAYS a matter of successive approximations to minimize the error surrounding the Truth. It's about getting at the Truth in something we are passionate about even though we need get there ... building knowledge is NOTHING BUT the process of making mistakes and the debate that surrounds mistake making.

3) In this Life, we are never in full possession of the Truth, ie the Science is NEVER settled -- but, at some point, the decision of what to print/publish/deploy has to be settled, although it will necessarily still contain non-fatal errors.

4) SAFETY FIRST ... if we are faced with releasing two candidates, we release the safer, cheaper, stabler, less prone-to-get-somebody-killed candidate as the Production release ... and we might make the Beta available, but ... consensus on the Production release is necessary on this because everyone involved at this point has to OWN the Production release, or else they cease being involved.  If the Production release proves to be unsafe, we rollback to what was safe.

## The DESIRE to become proficient in Git is an absolute REQUIREMENT for any contributor to GYGbe.

Nobody is proficient enough in Git ... and Git, itself, is still evolving and getting a lot better, every single day.  Contributing to GYGbe is one way to become more proficient in Git -- remember, also, that no one learns more in any teaching session than the person attempting to present material to an audience of demanding people who are intent on learning, ie nobody knows Git all ... but our intention, perhaps not the primary intention but definitely part of accomplishing any of our objectives, is to work at improving Git best practices, improving and contributing to the Git open source project, improve the planet of Git service providers, improve the overal galaxy of Git users.

You will change GYGbe by contributing merge requests. Thus, the STARTING ASSUMPTION of anyone wishing to contribute is that you understand, in theory, what basic processes in Git, such as Merge Requests are all about. Learn and internalize thinking in Git ... not for GYGbe ... for yourself, your professional future. There probably isn't a professional skill now that matters more than Git proficiency ... in the past maybe that required proficiency was some word processing or spreadsheet tool -- if you are serious about working with any kind of content, especially computer software or data, **Git is a bigger, more important deal than Excel or Word ever were.**  If you don't accept this as gospel, you shouldn't be even considering contributing to any GYGbe repositories.  




Security issues ... TBD

Pair programming, asynch.WORK, debugging together ... TBD

Root cause /full ishikawa ... TBD

Beyond CI/CD site reliability engineering is a massive opportunity to improve our monitoring, to immerse ourselves in the study of what might make this better, and above all to LEARN from the inevitable errors ... one has to LOVE failure ... TBD

The proprietary secret that so-called innovative companies gaurd as their "secret sauce" is that they don't want anyone to know how little they know ... we LIVE our OPEN values through OPEN SOURCE DevSecOps ... we fail in public so that other people can learn.  The ONLY thing that we KNOW is that we will have NO proprietary secrets ... TBD
