---
title: Job Postings Driven by Job Boards
subtitle: Progressive web app, exploiting data APIs
comments: false
---

Agile Data Science 2.0 using data APIs from existing sources, eg Indeed. The point of REV 1.0 of this app would be to put all data from existing job boards in a convenient dashboard hyperlinked ... other revs would be more ambitious.