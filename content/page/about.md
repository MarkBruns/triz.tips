---
title: About TRIZ.tips
subtitle: Assumptions on the rough schematic of the team creative processes that the TRIZ.tips decision support will be developed for 
comments: false
---

The creative processes of different inventors and engineering research and development teams will be radically different, but generally nowadays invention of complex products happens **in teams.**

TRIZ.tips can certainly be used by individuals, but it will be developed as something that a team would use, ie it might be much more formal and methodical than the approach that an individual creative artist would use. For example, we would foresee that TRIZ.tips solution would probably be consumed as a cloneable Git repository, of analysis tools like Jupyter noteboks that could run on an individual machine or on BinderHub.

In order to offer decision support or a knowledge engine to different kinds of inventive or problem-solving teams, we need to start with a very rough schematic of approximately what the creative problem-solving process by teams will look like:

1) Identification of needs and scope of problem
2) Analysis of these needs and implicit underlying assumptions
3) A review of existing solutions and applicable information
4) Brainstorming or formulation of lists of all possible solutions
5) Prioritized analysis of likely solutions and study of difference
6) Birth of a new idea or new set of ideas to test
7) Designed experimentation to test factor levels and confirmation of concept
8) Technical implementation and testing of alpha minimal viable product (MVP)
9) Review of results of test of alpha MVP with either complete re-examination starting at the first step OR refinement of MVP for either another round of alpha MVP testing or more detailed technical implementation of beta minimal viable product with actual users
10) Review of results of test of beta MVP with re-examination starting at prior step OR refinement of MVP for either another round of beta MVP testing or production launch.  
