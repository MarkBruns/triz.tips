---
title: Preliminary Anti-Action
subtitle: Use countermeasures to offset harmful effects.
date: 2020-02-26
tags: ["Preliminary Anti-Action", "Principles"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

Preliminary anti-action

* When necessary retain harmful effect to gain and useful effects, countermeasures offset/ameliorate harmful effects.
* Buffer a solution to prevent harm from extremes of pH.
* Create beforehand stresses in an object that will oppose known undesirable working stresses later on.
* Pre-stress rebar before pouring concrete.
* Masking anything before harmful exposure: Use a lead apron on parts of the body not being exposed to X-rays. Use masking tape to protect the part of an object not being painted
 