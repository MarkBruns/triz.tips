---
title: Segmentation
date: 2020-01-01
---

Divide an object into independent parts.
* forest, tree, branch, twig, leaf, cell ...
* seed coat, cotyledon, root, shoot ...

Use a work breakdown structure for a large project.
* Make an object easy to disassemble.
* Modular furniture
* Quick disconnect joints in plumbing

Increase the degree of fragmentation or segmentation.
* Replace solid shades with Venetian blinds.
* Use powdered welding metal instead of foil or rod to get better penetration of the joint.