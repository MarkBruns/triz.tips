---
title: Continuity of useful action
subtitle: To the extent possible, force all parts of the system to be employed at full load, all the time
date: 2020-05-06
tags: ["Continuity", "Principles"]
---


Continuity of useful action

* Shared cloud computing resources
* Structure overwinters, then leafs out in spring to rapidly achieve solar canopy
* Flywheel (or hydraulic system) stores energy when a vehicle stops, so the motor can keep running at optimum power.
* Run the bottleneck operations in a factory continuously, to reach the optimum pace. (From theory of constraints, or takt time operations)
* Eliminate all idle or intermittent actions or work.
* Print during the return of a printer carriage–dot matrix printer, daisy wheel printers, inkjet printers.