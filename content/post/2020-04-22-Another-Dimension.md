---
title: Another Dimension
subtitle: Always Be Flanking -- when going Through is out, try Up, Around, Over, Under, RF, Virtual,
date: 2020-04-22
tags: ["AnotherDimension", "Principles"]
---

Another Dimension

* Move FROM trad 3D meatspace to the virtual realm.
* Infrared computer mouse moves in space, instead of on a surface, for presentations.
* Five-axis cutting tool can be positioned where needed.
* Use a multi-story arrangement of objects instead of a single-story arrangement.
* Cassette with 6 CD’s to increase music time and variety
* Electronic chips on both sides of a printed circuit board
* Employees “disappear” from the customers in a theme park, descend into a tunnel, and walk to their next assignment, where they return to the surface and magically reappear.
* Tilt or re-orient the object, lay it on its side.
* Use ‘another side’ of a given area.
* Stack microelectronic hybrid circuits to improve density.