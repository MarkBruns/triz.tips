---
title: Lie Groups in Patent AI/ML
subtitle: Smooth, continuous, mathematical symmetry groups of applied operators
date: 2020-05-01
tags: ["AI/ML", "Theory"]
comments: false
---

TRIZ.tips is an attempt to bring the general notions of brute-force patent comparison to the AI/ML task of ideation using existing databases of things like patents ... it's about suggesting ways to narrow the search for the needle in the haystack ... TRIZ is the application of analogous conceptual thinking to a problem that might not appear to be so analogous on the surface.

One of our areas for study is [Lie Theory](https://en.wikipedia.org/wiki/Lie_theory) or [Lie Algebra](https://en.wikipedia.org/wiki/Lie_algebra) or [Lie Groups](https://en.wikipedia.org/wiki/Lie_group) and [Symmetry Groups](https://en.wikipedia.org/wiki/Symmetry_group) ... the intuition is that we have this similarity of properties in these problems, so what might be a quasi-symmetric or approximately symmetric operation to perform to make the solution more obvious ... given this complex thing, how might we fold/knot/unfold/unknot it to make a solution simpler.