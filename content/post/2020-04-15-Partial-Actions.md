---
title: Partial Actions
subtitle: Overspray, remove excess
date: 2020-04-15
tags: ["PartialActions", "Principles"]
---

Partial and Excessive Actions

* Come at the problem in different ways, maybe in steps
* If 100 percent of an object is hard to achieve using a given solution method then, by using ‘slightly less’ or ‘slightly more’ of the same method, the problem may be considerably easier to solve.
* Over spray when painting, then remove excess. (Or, use a stencil–this is an application of Principle 3, Local Quality and Principle 9, Preliminary anti-action).
* Fill rapidly, then slowly “top off” when filling the gas tank of your car.
 