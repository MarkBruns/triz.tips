Collect The Ideas in #Brainstorm Post

Group those 200 Ideas Under Category Headings

A ... Do Right Away
B ... Get The Prereqs Then Do Right Away
C ... Maybe -- Reformulate / Rescope The Issue
D ... Wild Ass Parking Lot

Start with [Book](bookURL) and then create a new notebook in [Google Colaboratory](https://colab.research.google.com/notebooks/intro.ipynb#scrollTo=OwuxHmxllTwN)

# WHY do you want to delve into this topic?  

# What did you learn, eg, disappointments, WOWs, aggravations ... that you did not expect to learn from this topic?